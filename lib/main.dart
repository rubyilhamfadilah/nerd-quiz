import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nerdquizapp/pages/home_page.dart';
import 'package:nerdquizapp/pages/result_page.dart';
import 'package:nerdquizapp/pages/test_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.orange),
      home: Homepage(),
    );
  }
}
