import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nerdquizapp/models/question_model.dart';
import 'package:nerdquizapp/pages/test_page.dart';
import 'package:http/http.dart' as myHttp;

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  late QuestionModel questionModel;
  TextEditingController usernameController = TextEditingController();

  final String url =
      "https://script.google.com/macros/s/AKfycbxqT9Ke_HviAaXpQKgsEK4r3HIf5oPiLQFDoQ_bqZ3SPIBVvwnLwbG7SzuSs__rBWSYWg/exec";

  void getAllData(String username) async {
    try {
      var response = await myHttp.get(Uri.parse(url));
      questionModel = QuestionModel.fromJson(json.decode(response.body));
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => TestPage(
                questionModel: questionModel,
                username: username,
              )));
    } catch (err) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple,
      body: SafeArea(
          child: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            "Yuk Quiz",
            style: GoogleFonts.poppins(fontSize: 26, color: Colors.white),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: usernameController,
              decoration: InputDecoration(
                  hintText: "Masukan username",
                  fillColor: Colors.white,
                  filled: true),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                getAllData(usernameController.text);
              },
              child: Text("M U L A I"))
        ]),
      )),
    );
  }
}
