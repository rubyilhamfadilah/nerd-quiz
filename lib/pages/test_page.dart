import 'package:countdown_progress_indicator/countdown_progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nerdquizapp/models/question_model.dart';
import 'package:nerdquizapp/pages/result_page.dart';

class TestPage extends StatefulWidget {
  final QuestionModel questionModel;
  final String username;
  const TestPage(
      {Key? key, required this.questionModel, required this.username})
      : super(key: key);

  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  final _controller = CountDownController();
  int index = 0;
  int result = 0;

  void navigate(String optionChar) {
    setState(() {
      if (optionChar == (widget.questionModel.data[index].correctOption)) {
        result++;
      }
      index++;
      if (index == widget.questionModel.data.length) {
        Navigator.of(context)
            .push(MaterialPageRoute(
                builder: (context) => ResultPage(
                      result: result,
                    )))
            .then((value) {
          setState(() {});
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple[400],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    '${index + 1} / ${widget.questionModel.data.length.toString()}',
                    style:
                        GoogleFonts.poppins(fontSize: 20, color: Colors.white)),
                Text(
                  widget.username,
                  style: GoogleFonts.poppins(fontSize: 20, color: Colors.white),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 150,
            width: 150,
            child: CountDownProgressIndicator(
              controller: _controller,
              valueColor: Color.fromARGB(255, 255, 123, 0),
              backgroundColor: Colors.white,
              initialPosition: 0,
              duration: 60,
              text: 'Detik',
              onComplete: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(
                        builder: (context) => ResultPage(
                              result: result,
                            )))
                    .then((value) {
                  setState(() {});
                });
              },
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: Text(
              widget.questionModel.data[index].question,
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(fontSize: 22, color: Colors.white),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          GestureDetector(
            onTap: () {
              navigate("a");
            },
            child: OptionWidget(
              color: Colors.red,
              optionChar: "A.",
              optionDetail: widget.questionModel.data[index].optionA,
            ),
          ),
          GestureDetector(
            onTap: () {
              navigate("b");
            },
            child: OptionWidget(
                color: Colors.orange,
                optionChar: "B.",
                optionDetail: widget.questionModel.data[index].optionB),
          ),
          GestureDetector(
            onTap: () {
              navigate("c");
            },
            child: OptionWidget(
                color: Colors.green,
                optionChar: "C.",
                optionDetail: widget.questionModel.data[index].optionC),
          ),
          GestureDetector(
            onTap: () {
              navigate("d");
            },
            child: OptionWidget(
                color: Colors.blue,
                optionChar: "D.",
                optionDetail: widget.questionModel.data[index].optionD),
          ),
        ],
      )),
    );
  }
}

class OptionWidget extends StatelessWidget {
  final String optionChar;
  final String optionDetail;
  final Color color;

  const OptionWidget({
    required this.optionChar,
    required this.optionDetail,
    required this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: [
              Text(
                optionChar,
                style: GoogleFonts.poppins(fontSize: 20, color: Colors.white),
              ),
              SizedBox(
                width: 30,
              ),
              Expanded(
                child: Text(
                  optionDetail,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.poppins(fontSize: 20, color: Colors.white),
                ),
              )
            ],
          ),
        ),
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(10)),
      ),
    );
  }
}
